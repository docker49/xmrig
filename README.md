# xmrig

## Installation

Modify the .env in order to define:

* XMRIG_VERSION: The version of the software to deploy
* XMRIG_WALLET: Your wallet address
* XMRIG_POOL: The pool you want to join

Configuration file must be located in config/config.json.
You can create configuration file using <https://config.xmrig.com>
or use the one provided in this repository. If you create it, replace
the line with user and url in order to have:

```json
    "url": "XMRIG_POOL",
    "user": "XMRIG_WALLET",
```

Create a docker image using the command:

```bash
docker-compose build
```

## Start the container

```bash
docker-compose up -d
```


## Get logs from the container

```bash
docker-compose logs -f --tail=10
```
